var date = {
  day: 8,
  month: 3,
  format: function()
  {
    var d = (this.day<10 ? '0' : '') + this.day;
    var m = (this.month<10 ? '0' : '') + this.month;
    return d + '.' + m;
  }
};

switch (date.format())
{
  case '23.02':
    console.log('С 23 февраля!');
    break;
  case '08.03':
    console.log('С 8 марта!');
    break;
  case '31.12':
    console.log('С новым годом!');
    break;
  default:
    console.log('Сегодня просто отличный день и безо всякого праздника');
    break;
}