document.getElementById('btn').onclick = function()
{
  var age = document.getElementById('age').value;

  if (isNaN(Number(age))) console.log('Ошибка: некорректный ввод.'); else
  {
    if (age>=0 && age<=17)
      console.log('Вы слишком молоды.');
    else if (age>=18 && age<=60)
      console.log('Отличный возраст');
    else if (age>=61)
      console.log('Привет, бабуля!');
    else
      console.log('Ошибка: возраст не бывает отрицательным.');
  }
};